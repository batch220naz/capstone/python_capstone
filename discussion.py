# Specifications
# 1. Create a Person class that is an abstract class that has the following methods
#   a. getFullName method
#   b. addRequest method
#   c. checkRequest method
#   d. addUser method

# 2. Create an Employee class from Person with the following properties and methods
#   a. Properties(make sure they are private and have getters/setters)
#       firstName, lastName, email, department
#   b. Methods
#       Abstract methods
#       For the checkRequest and addUser methods, they should do nothing.
#       login() - outputs "<Email> has logged in"
#       logout() - outputs "<Email> has logged out"
#       Note: All methods just return Strings of simple text
#           ex. Request has been added

# 3. Create a TeamLead class from Person with the following properties and methods
#   a. Properties(make sure they are private and have getters/setters)
#       firstName, lastName, email, department, members
#   b. Methods
#       Abstract methods
#       For the addRequest and addUser methods, they should do nothing.
#       login() - outputs "<Email> has logged in"
#       logout() - outputs "<Email> has logged out"
#       addMember() - adds an employee to the members list
#       Note: All methods just return Strings of simple text
#           ex. Request has been added

# 4. Create an Admin class from Person with the following properties and methods
#   a. Properties(make sure they are private and have getters/setters)
#       firstName, lastName, email, department
#   b. Methods
#       Abstract methods
#       For the checkRequest and addRequest methods, they should do nothing.
#       login() - outputs "<Email> has logged in"
#       logout() - outputs "<Email> has logged out"
#       addUser() - outputs "New user added"
#       Note: All methods just return Strings of simple text
#           ex. Request has been added

# 5. Create a Request that has the following properties and methods
#   a. properties
#       name, requester, dateRequested, status
#   b. Methods
#       updateRequest
#       closeRequest
#       cancelRequest
#       Note: All methods just return Strings of simple text
#           Ex. Request < name > has been updated/closed/cancelled

# Test cases:
employee1 = Employee("John", "Doe", "djohn@mail.com", "Marketing")
employee2 = Employee("Jane", "Smith", "sjane@mail.com", "Marketing")
employee3 = Employee("Robert", "Patterson", "probert@mail.com", "Sales")
employee4 = Employee("Brandon", "Smith", "sbrandon@mail.com", "Sales")
admin1 = Admin("Monika", "Justin", "jmonika@mail.com", "Marketing")
teamLead1 = TeamLead("Michael", "Specter", "smichael@mail.com", "Sales")
req1 = Request("New hire orientation", teamLead1, "27-Jul-2021")
req2 = Request("Laptop repair", employee1, "1-Jul-2021")

assert employee1.getFullName() == "John Doe", "Full name should be John Doe"
assert admin1.getFullName() == "Monika Justin", "Full name should be Monika Justin"
assert teamLead1.getFullName() == "Michael Specter", "Full name should be Michael Specter"
assert employee2.login() == "sjane@mail.com has logged in"
assert employee2.addRequest() == "Request has been added"
assert employee2.logout() == "sjane@mail.com has logged out"

teamLead1.addMember(employee3)
teamLead1.addMember(employee4)
for indiv_emp in teamLead1.get_members():
    print(indiv_emp.getFullName())

assert admin1.addUser() == "User has been added"

req2.set_status("closed")
print(req2.closeRequest())
